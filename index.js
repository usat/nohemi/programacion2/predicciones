const key = "472b6baafe8ffa76df521fdf39a74050"
let url = "https://api.openweathermap.org/data/2.5/"

let buscar = document.getElementById("btnbuscar")

buscar.onclick = () => {
	ahora()
	prediccion()
}

function prediccion() {
	let ciudad = $("#txtbuscar").val()
	$.ajax({
		url: `${url}forecast`,
		dataType: "json",
		type: "GET",
		data: {
			q: `${ciudad},PE`,
			appid: key,
			units: "metric",
			cnt: "20"
		},
		success: function(data) {
			console.log("Received data:", data)
			$("#lblciudad").text(data.city.name)
			var wf = ""
			wf += "<h4>Temperatura Futura</h4>"
			$.each(data.list, function(index, val) {
				wf +=
					"<div class='text-center' style='border: 1px solid black; max-width: max-content; border-radius: 10px; padding: 20px;margin-left: 25%;margin-bottom: 20px;'>"
				wf += "<p><b> " + val.dt_txt + "</b></p>"
				wf +=
					"<p style='margin: 0 !important'><b>Temperatura Min:</b> " +
					val.main.temp_min +
					"&degC </p>"
				wf +=
					"<p style='margin: 0 !important'><b>Temperatura Max:</b> " +
					val.main.temp_max +
					"&degC </p>"
				wf += "<b><span>" + val.weather[0].description + "</span></b>"
				wf +=
					"<img src='https://openweathermap.org/img/w/" +
					val.weather[0].icon +
					".png'>"
				wf += "</div>"
			})
			$("#weatherFuture").html(wf)
		}
	})
}

function ahora() {
	let ciudad = $("#txtbuscar").val()
	$.ajax({
		url: `${url}weather`,
		dataType: "json",
		type: "GET",
		data: {
			q: `${ciudad},PE`,
			appid: key,
			units: "metric"
		},
		success: function(data) {
			// console.log("Received data:", data)
			var wf = ""
			wf += "<h4>Temperatura Actual</h4>"
			wf +=
				"<div class='text-center' style='border: 1px solid black; max-width: max-content; border-radius: 10px; padding: 20px;margin-left: 25%;'>"
			wf +=
				"<p><b>" +
				new Date(data.dt * 1000)
					.toISOString()
					.slice(0, 19)
					.replace("T", " ") +
				"</b></p>"
			wf +=
				"<p style='margin: 0 !important'><b>Temperatura Min:</b> " +
				data.main.temp_min +
				"&degC </p>"
			wf += "<p><b>Temperatura Max:</b> " + data.main.temp_max + "&degC </p>"
			wf += "<b><span>" + data.weather[0].description + "</span></b>"
			wf +=
				"<img src='https://openweathermap.org/img/w/" +
				data.weather[0].icon +
				".png'>"
			wf += "</div>"
			$("#weatherNow").html(wf)
		}
	})
}
